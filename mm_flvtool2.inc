<?php



/**
 * reads meta tags from a flv file
 * @param $file_path is the path to the file
 * @return is the parsed xml from the command, false if there is an error
 */
function mm_flvtool2_get_tags($file_path) {  
  // get tags
  $output = mm_flvtool2_run_command($file_path, 'Px');
  // check output
  if ($output) {
    $tags = mm_flvtool2_parse_output($output);
    return $tags;
  }
  return false;
}

/**
 * writes meta tags to an flv file
 * @param $file_path is the path to the file
 * @return is the parsed xml from the command, false if there is an error
 */
function mm_flvtool2_write_tags($file_path) {
  $output = mm_flvtool2_run_command($file_path, 'UPx');
  if ($output) {
    $tags = mm_flvtool2_parse_output($output);
    return $tags;
  }
  return false;
}


/**
 * takes the XML output from flvtool2 and returns
 * a structured array of index and values to the xml
 * @param $output is the output from the flvtool2 command, assumes xml 
 * @return array of the xml index and the xml values
 */
function mm_flvtool2_parse_output($output) {
  $output = implode("\n", $output);
  $parse = xml_parser_create();
  xml_parse_into_struct($parse, $output, $values, $index);
  xml_parser_free($parse);

  // returning one array of indexes => values
  foreach($index as $attribute => $keys) {
  	foreach($keys as $i => $key) {
      $combined[strtolower($attribute)][$i] = $values[$key];
    }
  }
  return $combined;
}

/**
 * this executes the flvtool command
 * @param $file_path is the file to perform the operation on
 * @param $options are the optioons executed with
 * @return output from the command
 */
function mm_flvtool2_run_command($file_path, $options) {
  $output = array();
  $return_var = 0;
  
  // get paths
  $command[] = variable_get('mm_flvtool2_ruby_path', '/usr/bin/ruby');
  $command[] = variable_get('mm_flvtool2_path', '/System/Library/Frameworks/Ruby.framework/Versions/1.8/usr/bin/flvtool2');  
  // options
  $command[] = '-'. $options;
  // file
  $command[] = '"'. $file_path .'"';
  // output
  $command[] = "2>&1"; 
 
  // run the command 
  exec(implode(' ', $command), $output, $return_var);

  if ($return_var) {
    $message = t("Trouble running command: %cmd.  Output was %output.  Return_var was %return for file %file.",
      array('%cmd' => $command,
        '%output' => implode(', ', $output),
        '%return' => $return_var,
        '%file' => $file_path));    
    watchdog('mm_flvtool2', $message);
  }
  else {
    return $output;
  }
}


/**
 * gets tags from a given file, caches them locally
 * @param string $filepath is path to file
 * @param string $param is parameter value to return
 * @return parameter
 */
function mm_flvtool2_get_tags($filepath, $param) {
	static $file_parameters;		
	if (! $file_parameters[$filepath]) {	  
		$file_parameters[$filepath] = mm_flvtool2_get_tags($filepath); 
	}
	// get the index value so we can find it in the values list
	$index_value = $file_parameters[$filepath]['index'][$param][0];
	return $file_parameters[$filepath]['values'][$index_value]['value'];
}
