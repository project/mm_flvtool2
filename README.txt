
The mm_flvtool2 module enables meta data writing for flv files. This
is helpful for getting file lengths, and other interaction with the 
flv file.

-----------------------------------------------------------------------
 INSTALLATION
-----------------------------------------------------------------------
This module relies on Ruby and FLVTool2 to write meta data
Download and install Ruby and  FLVTool2 on your server first:  
http://www.osflash.org/flvtool2

Next, install the module under sites/all/modules or 
sites/yoursite/modules.  How to install:  http://drupal.org/node/120641

Go to admin/build/modules

Enable the mm_flvtool2 module


-----------------------------------------------------------------------
 CONFIGURATION
-----------------------------------------------------------------------

Go to admin/media_mover/settings
These are the global settings for the Media Mover modules

Make sure you configure the path to Ruby. This is relative to the root 
of your server. If don't know where Ruby is installed and you have
access to the command line of your server, you can run:

#which ruby


-----------------------------------------------------------------------
 USAGE
-----------------------------------------------------------------------

Goto admin/media_mover to build a configuration

Select which files to harvest in the 'harvest configuration' section.  
Do not set it to 'Bypass this operation',
otherwise flvtool2 module will not process any files.

In 'process configuration' section, select:

FLVTool2: Add FLV meta data
 
Click Save Configuration, and it is ready to go.
  
Media Mover configurations will be run every time cron is run.
You can run a single configuration by hand by clicking 'run' at 
admin/media_mover for the specified configuration.

